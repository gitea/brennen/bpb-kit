# This file contains common shell defaults, mostly paths and aliases/functions,
# used by my configs for Bash, ZSH, and Xmonad.
#
# See also:
#
#   ~/.zshrc
#   ~/.bashrc
#   ~/bin/xmonad.start

# Path handling: {{{
#
# This accounts for, at least:
#
#   - a custom Firefox install
#   - some bin dirs in my homedir
#   - golang, ruby, rust
#
# .sh_common is sourced by ~/bin/xmonad.start to provide for entire x session
# getting these vars.
#
# Beware that other modifications to path might be made in .zshrc or .bashrc
# It's a tangled mess which certainly warrants a TODO.

export GOPATH=~/code/go
export GOROOT=/usr/local/go
export GEM_HOME=~/gems
export P1K3_ROOT=~/workspace/p1k3

# Mostly likely stolen from thcipriani: Put a dir at the start of path, IFF
# it's not already in the path:
prepend_to_path () {
  local dir="$1"

  if [[ -d "${dir}" ]]; then
    if [[ ":$PATH:" != *"${dir}"* ]]; then
      export PATH="${dir}:${PATH}"
    fi
  fi
}
prepend_to_path "$GOROOT/bin" # golang
prepend_to_path "$GOPATH/bin" # golang
prepend_to_path "$GEM_HOME/bin" # ruby
prepend_to_path "$HOME/.cargo/bin" # rust
prepend_to_path "$HOME/.cabal/bin" # haskell
prepend_to_path ~/.local/bin
prepend_to_path ~/.xmonad
prepend_to_path ~/notes/bin
prepend_to_path ~/firefox
prepend_to_path ~/.fzf/bin
prepend_to_path ~/bin

# }}}

# General environment / config: {{{
# Debian stuff - define DEBFULLNAME, DEBEMAIL here:
if [ -f ~/.sh_common_debconfig ]; then
  . ~/.sh_common_debconfig
fi

# Explicitly set default editor:
export EDITOR=$(which vim)

# Configure default less(1) options:
export LESS='-FiRSX'

# Configure fzf fuzzyfinder:
export FZF_CTRL_R_OPTS='-e' # exact match by default - see fzf(1)

# For mediawiki/core/docker-compose.yml
export MW_DOCKER_UID=$(id -u)
export MW_DOCKER_GID=$(id -g)

# }}}

# Aliases, various and sundry: {{{

alias ac='apt-cache'
alias agu='sudo apt-get update && sudo apt-get upgrade'
alias c='commandlog'
alias g='git'
alias gitsu='git submodule sync && git submodule update'
alias m='make'
alias mk='marks'
alias mka='marks add'
alias s='sudo'
alias v='vim'
alias n='notes'
alias vt='vim $(todaydir)'

# How big is the terminal?
alias dim='echo $(tput cols)x$(tput lines)'

# vi life:
alias :e="$EDITOR"
alias :q='exit'
alias :wq='exit'
alias :r='cat'

# }}}

# Functions: {{{

# p1k3 navigation:
function p {
  cd "$P1K3_ROOT"
}

# Current p1k3 day directory, if it exists:
function ct {
  cd "$(todaydir)"
}

if type exa &> /dev/null; then
  # If exa is installed, replace the traditional ls aliases with exa
  # invocations.

  function l {
    exa --group-directories-first $@
  }

  function ll {
    exa --group-directories-first -l --git $@
  }

  function la {
    # This would typically be `ls -A`, for "almost all", which excludes . and ..
    # - exa appears to do that by default, and so doesn't have an -A option
    exa --group-directories-first -a $@
  }

  function lal {
    # ls -Al
    exa --group-directories-first -al --git $@
  }

  function lah {
    # ls -Alh
    # exa uses human-readable sizes by default, so doesn't have the -h flag.
    exa --group-directories-first -al --git $@
  }
else
  # No exa, so set up traditional ls aliases as functions.
  function l {
    ls --group-directories-first --color -CF $@
  }

  function ll {
    ls --group-directories-first -l --color $@
  }

  function la {
    # Almost all - exclude . and ..:
    ls --group-directories-first -A $@
  }

  function lal {
    ls --group-directories-first -Al $@
  }

  function lah {
    ls --group-directories-first -Alh $@
  }
fi

# Get a persistent directory history menu in fzf - originally written for dmenu
# and inspired by jholland at:
#
#   http://hints.macworld.com/article.php?story=20050806202859392
#
# Includes a preview of the directory using ls.
#
# See: https://p1k3.com/2016/5/17/
function h {
  if [ ! -z "$@" ]; then
    # If we got arguments, pass them in as a starting query:
    cd "$(_h_fzf -q $@)"
  else
    cd "$(_h_fzf)"
  fi
}

# Helper for h, above
function _h_fzf {
  tail -2500 ~/.directory_history \
    | tac \
    | unsorted-unique \
    | fzf --no-sort --height=50% --preview="ls -CF {}" \
    $@
}

# Bookmark a spot:
function b {
  if [ ! -z "$@" ]; then
    echo "$(realpath "$@")" >> ~/.directory_bookmarks
  else
    cd "$(sort ~/.directory_bookmarks | uniq | fzf --no-sort --height=50%)"
  fi
}

# Create a directory (if it doesn't exist) and cd to it:
function mcd {
  mkdir -p "$1" && cd "$1"
}

# Use official git prompt, if it exists, or fall back to
# parse_git_branch:
if [ -f /usr/lib/git-core/git-sh-prompt ]; then
  GIT_PS1_SHOWCOLORHINTS=1
  GIT_PS1_SHOWUNTRACKEDFILES=1
  GIT_PS1_SHOWDIRTYSTATE=1
  GIT_PS1_SHOWUPSTREAM="auto verbose"
  GIT_PS1_SHOWSTASHSTATE=1

  . /usr/lib/git-core/git-sh-prompt
else
  # Emulate __git_ps1 interface which takes 2 params - before part
  # and after part:
  function __git_ps1 {
    PS1="$1 $(parse_git_branch)$2"
  }
fi

# Get some information about a git repo
function parse_git_branch {
  # if [ ! -z "$has_git_prompt" ]; then
  #   __git_ps1
  #   return
  # fi
  
  # Formerly:
  # ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  # echo "("${ref#refs/heads/}") "

  # Originally borrowed from:
  # https://github.com/robacarp/config_files/commit/5d983240d509bf4f7815ed822c0e868ccce08a79
  git_status=$(git status --porcelain -b 2> /dev/null) || return

  echo $git_status | awk '
    BEGIN {
      status["untracked"] = 0
      status["mods"] = 0
      branch_status = ""
    }
    $0 ~ /.*\[.*\]$/ {
      branch_status = " " $3 " " $4
    }
    $1 ~ /##/ {
      gsub(/\.\.\..*/, "")
      branch_name = $2
    }
    $1 ~ /\?\?/       { status["untracked"] ++ }
    $1 ~ /M/          { status["mods"] ++      }

    END {
      printf "(%s m%i/u%i%s) ", branch_name, status["mods"], status["untracked"], branch_status
    }
  '
}

# Render a simple horizontal rule / title bar in the terminal.  Again, a
# variation on robacarp:
# https://robacarp.io/2018/07/19/bash-hr-function-to-draw-a-line-across-the-terminal.html
function titlebar {
  # Text written into the horizontal rule, left justified
  text=${1:-}
  length=$(echo "$text" | wc -m)

  # set the color
  echo -e -n "\033[30;47m"

  # print the message
  echo -n "$text"

  # finish the line across the console
  cols=$(expr "$(tput cols)" - $length)
  printf " %${cols}s"

  # clear the background color and start a new line
  echo -e "\033[0m"
}

# }}}
