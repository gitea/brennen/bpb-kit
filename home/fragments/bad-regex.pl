#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;

my $intentionally_broken_regex = '[';
my $compiled_regex;
eval {
  $compiled_regex = qr/$intentionally_broken_regex/;
};
if ($@) {
  # There should be an error, so let's just quote everything:
  $compiled_regex = qr/\Q$intentionally_broken_regex\E/
}
