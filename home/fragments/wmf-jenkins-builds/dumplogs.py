#!/usr/bin/python3

import requests

# r = requests.get('https://integration.wikimedia.org/ci/job/mediawiki-quibble-vendor-mysql-php81/api/json')

jobs = ['mediawiki-quibble-vendor-mysql-php81',  'mediawiki-quibble-vendor-mysql-php82', 'mediawiki-quibble-vendor-mysql-php74']

for job in jobs:
    r = requests.get('https://integration.wikimedia.org/ci/job/' + job + '/api/json?tree=allBuilds[number,timestamp,url,duration,result,runs[url,number],actions[foundFailureCauses[*]]]')

    data = r.json()

    for build in data['allBuilds']:
        console_log_url = build['url'] + 'logText/progressiveText?start=0'
        print(console_log_url)

        log_r = requests.get(console_log_url)
        log_text = log_r.text
        log_filename = 'logs/' + job + '-' + str(build['number'])
        with open(log_filename, 'w') as f:
            f.write(log_text)
