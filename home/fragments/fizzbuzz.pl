#!/usr/bin/env perl

# Write a program that prints the numbers from 1 to 100. But for multiples of
# three print "Fizz" instead of the number and for the multiples of five print
# "Buzz". For numbers which are multiples of both three and five print
# "FizzBuzz".

for (1..100) {
  my $no_number = 0;
  unless ($_ % 3) {
    print "Fizz";
    $no_number = 1;
  }
  unless ($_ % 5) {
    print "Buzz";
    $no_number = 1;
  }
  print $_ unless $no_number;
  print "\n";
}
