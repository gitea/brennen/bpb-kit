#!/usr/bin/perl

while (<>) {
  chomp;
  my @all = split /,/;
  my $headword = shift @all;
  @all = grep { $_ !~ /e/i } @all;
  print join(',', $headword, @all) . "\n";
}
