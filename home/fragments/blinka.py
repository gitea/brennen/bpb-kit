import board
import digitalio
import busio

print("Check on the mic")

# Try creating digital input:
pin = digitalio.DigitalInOut(board.D4)
print("Digital IO ok!")

# I2C
i2c = busio.I2C(board.SCL, board.SDA)
print("I2C ok!")

# SPI
spi = busio.SPI(board.SCLK, board.MOSI, board.MISO)
print("SPI ok!")

print("done!")
