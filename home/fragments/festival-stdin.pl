#!/usr/bin/env perl

use warnings;
use strict;

# Get festival to do text-to-speech on stdin, one line a time.  Useful with
# tail -f.  (One child process per line.)

while (<>) {
  open my $festival, "| festival --tts" or die "can't fork: $!";
  print $festival $_;
  close $festival;
}
