#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;
use Time::Piece;

my $specific_time = Time::Piece->strptime('2020-11-06 20:20:01', "%Y-%m-%d %T");
say $specific_time;

my $now = localtime();
say $now->epoch;

my $hour_ago = localtime()->epoch - 3600;
say $hour_ago;
