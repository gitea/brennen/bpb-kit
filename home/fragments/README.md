This is a place to stash random code snippets, test cases, etc., that don't
quite rise to the level of usable software, but might be useful to remember in
future.

I added it to this repository when I noticed that I was always opening random
little files called things like `test.sh` to check on some technique or work
through a small problem.
