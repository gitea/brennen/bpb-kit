#!/bin/sh
pandoc -f commonmark ./flush-headers.md
pandoc -f gfm ./flush-headers.md
pandoc -f markdown ./flush-headers.md
pandoc -f markdown_github ./flush-headers.md
pandoc -f markdown_mmd ./flush-headers.md
pandoc -f markdown_phpextra ./flush-headers.md
pandoc -f markdown_strict ./flush-headers.md
pandoc -f native ./flush-headers.md
