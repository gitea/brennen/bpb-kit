#!/bin/sh
mkdir /tmp/gitamend
cd /tmp/gitamend

touch foo
git init
git add foo
git commit -m 'an initial commit'
git show HEAD

git commit --amend --date='1941-12-07T12:00:00+00' --no-edit
git show HEAD

rm -rf /tmp/gitamend
