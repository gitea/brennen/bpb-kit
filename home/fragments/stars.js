#!/usr/bin/env node

process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function(data) {
  var stars = data.replace(/\S/g, '★');
  process.stdout.write(stars);
});
