#!/bin/sh

# Generate a list of used binary names in Debian.

# There is definitely a better way to go about this.

# Make sure we've got apt-file and lz4 compression utils:
sudo apt install apt-file lz4
sudo apt-file update

cd /var/lib/apt/lists || exit 1
lz4cat ./*.lz4 | \
  grep -E '^(usr/bin/|sbin/|bin/|usr/games)' | \
  cut -f1 -d' ' | \
  perl -pe 's/^(.*)\/(.*)$/$2/' | \
  sort | uniq > ~/used_names.txt
