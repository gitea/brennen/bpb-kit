#!/bin/sh

# This will print the files with the substituions done:
perl -pe 's/"(\d{4}-\d\d-\d\dT\d\d:\d\d:\d\dZ)"/$1/' ./*.md

# This would actually do the substitutions inline:
# perl -ie 's/"(\d{4}-\d\d-\d\dT\d\d:\d\d:\d\dZ)"/$1/' ./*.md
