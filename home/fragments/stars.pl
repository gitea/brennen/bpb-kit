#!/usr/bin/env perl

use warnings;
use strict;

while (my $line = <>) {
  $line =~ s/\S/★/g;
  print $line . "\n";
}
