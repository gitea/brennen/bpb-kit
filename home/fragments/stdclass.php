<?php

class foo {
}

$bar = new foo();

if ($bar instanceof stdClass) {
  print "stdClass is a base class class for objects.";
} else {
  print "stdClass is not a base class class for objects.";
}
