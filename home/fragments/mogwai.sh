#!/usr/bin/env bash

if [ "$(date '+%H')" -lt 6 ]; then
  echo 'do not feed the mogwai'
else
  echo 'gizmo can have snacks'
fi
