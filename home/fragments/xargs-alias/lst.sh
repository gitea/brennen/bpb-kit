#!/usr/bin/env bash
echo -n '' > files.lftp
sed "s/'/\\\'/" example_selection | while read -r fname; do
  printf "put '%s'\n" "$fname" >> files.lftp
done
