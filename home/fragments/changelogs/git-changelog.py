#!/usr/bin/env python3

import subprocess
import debian.changelog

def git(commands):
    proc = subprocess.Popen(['git'] + commands,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)
    stdout, stderr = proc.communicate()

    return stdout

ch = debian.changelog.Changelog()

commit_log = git(['log'])
print(commit_log)

ch.new_block(
    package='example',
    version='0.1',
    distributions='unstable',
    urgency='low',
    author="%s <%s>" % debian.changelog.get_maintainer(),
    date=debian.changelog.format_date()
)
ch.add_change('')
print(ch)


