#!/usr/bin/env python
# encoding: utf-8

import sys
import re

write = sys.stdout.write
line = sys.stdin.readline()

while line:
  stars = re.sub('\S', '★', line)
  write(stars)
  line = sys.stdin.readline()
