#!/bin/sh

# https://mspsocial.net/@earthtopus/103286059506297173

echo "original letter frequency:"
grep -o . pangram.txt | tr -d '[:punct:]' | grep -v "^\s*$" | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -rn

echo
echo "lightly golfed:"
grep -oE '[[:alpha:]]' pangram.txt | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -rn

echo "original letter-only character count:"
cat pangram.txt | tr -cd '[:alpha:]' | wc -m

echo
echo "lightly golfed:"
tr -cd '[:alpha:]' < pangram.txt | wc -m
