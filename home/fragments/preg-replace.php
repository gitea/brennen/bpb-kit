<?php
$url = 'https://www.mouser.com/images/keystoneelectronics/images/Battery_1047_SPL.jpg';
print $url . "\n";

print preg_replace('
  {
    ^              # start of line
      (.*images/)  # domain etc.
      (.*)/images/ # middle bit
      (.*)         # image filename
    $              # EOL

  }x',
  '$1$2/lrg/$3',
  $url
);
