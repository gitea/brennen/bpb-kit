#!/usr/bin/env zsh

autoload colors zsh/terminfo
colors

for foo in $fg; do
  echo $foo hi
done

for foo in $bg; do
  echo $foo hey;
done

export PS1='%F{214}%K{123}%m%k%f'
