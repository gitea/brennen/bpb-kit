#!/usr/bin/env perl

use POSIX qw(strftime);

my $timestamp = strftime "%Y-%m-%e_%H:%M:%S", localtime;
print $timestamp;
