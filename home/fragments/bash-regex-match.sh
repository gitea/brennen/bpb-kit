#!/bin/bash

server='# foo';
if [[ $server =~ ^# ]] ; then
  echo 'got a comment'
fi

server='foo';
if [[ $server =~ ^# ]] ; then
  echo 'no comment'
fi
