#!/usr/bin/env perl

use warnings;
use strict;
use Encode;
use 5.10.0;

while (<>) {
  say "length of raw input: " . length $_;
  my $decoded = decode('UTF-8', $_);
  say "length of decoded input: " . length $decoded;
  say "encoded() on decoded input: " . encode('UTF-8', $decoded);
  say "encode() on raw input: " . encode('UTF-8', $_);
}
