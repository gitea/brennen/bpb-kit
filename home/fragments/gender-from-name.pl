#!/usr/bin/perl 

# This dates, as best I can now remember, to some argument about the
# demographics of the SparkFun customer base, probably in the context of
# whether we needed to Google Analytics everything to know things about our
# customers.  I think my point was approximately "friends, we already know that
# most of our users are dudes and here is some rough confirmation of that
# painfully obvious statement".
#
# Expects https://metacpan.org/pod/Text::GenderFromName
#
# No strong beliefs about the nature or appropriate quantization of sex,
# gender, names, or human identity generally on the part of the author are
# represented by this code fragment.

use strict;
use warnings;
use Text::GenderFromName;
use 5.10.0;

my ($male, $female, $unknown) = (0, 0, 0);

while (my $name = <STDIN>) {
  chomp $name;
  my $gender = gender($name);
  if (! defined $gender) {
    $unknown++;
    next;
  }

  if ($gender eq 'm') {
    $male++;
  } elsif ($gender eq 'f') {
    $female++;
  }
}

say "male: $male, female: $female, unknown: $unknown";
