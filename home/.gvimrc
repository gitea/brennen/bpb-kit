" vb is needed to stop beep
set visualbell

" t_vb sets visual bell action, we're nulling it out here
set t_vb=

" font stuffs
" set guifont=Ubuntu\ Mono\ 14
" set guifont=Droid\ Sans\ Mono\ 13
" set guifont=Andale\ Mono\ 13
set guifont=DejaVu\ Sans\ Mono\ 13

" I hoped some of these might help with gvim rendering bugs - no dice:
" au FocusGained * :redraw!
" au TabEnter * :redraw!
" au WinEnter * :redraw!

" color scheme - differs for terminal vim; i have yet to find anything that
" works equally well for both:
" colorscheme inkpot
colorscheme hornet

" get a list of current toolbar definitions:
" :tmenu ToolBar

" remove some defaults from toolbar:
aunmenu ToolBar.Help
aunmenu ToolBar.FindHelp

" add a few things to toolbar:

tmenu ToolBar.nerdtree Toggle display of the NERDTree
400amenu ToolBar.nerdtree :NERDTreeToggle<CR>

tmenu ToolBar.nerdtreefind Find current file in NERDTree
410amenu ToolBar.nerdtreefind :NERDTreeFind<CR>

tmenu ToolBar.timeslice Timeslice for current file
420amenu ToolBar.timeslice :call BPB_TimesliceForFile()<CR>

" a separator - name must be unique:
amenu ToolBar.-Syntax- :

tmenu ToolBar.syntoggle Toggle Syntastic mode
520amenu ToolBar.syntoggle :SyntasticToggleMode<CR>

" a separator - name must be unique:
amenu ToolBar.-Vimwiki- :

tmenu ToolBar.vimwiki Open primary Vimwiki index
620amenu ToolBar.vimwiki :VimwikiIndex<CR>
