# Common path, aliases etc. for both shells I actually use:
# following directive just silences a warning:
# shellcheck source=/dev/null
source ~/.sh_common

# Don't put duplicate lines in the history, don't record commands with a leading space.
# See bash(1) for more options:
export HISTCONTROL=ignoreboth
export HISTFILE=~/.histfile
export HISTSIZE=-1
export HISTFILESIZE=-1

# Append to the history file, don't overwrite it:
shopt -s histappend
PROMPT_COMMAND="history -a; history -n; $PROMPT_COMMAND"

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS:
shopt -s checkwinsize

# Change to a directory with just its name:
shopt -s autocd

# Ideally we want to use this prompt:
#
# user@host hh:mm:ss cwd (git branch) $
#
# In xterms, we also set the window title to cwd.

# Ansi color escape sequences:
prompt_black='\[\e[30m\]'
prompt_red='\[\e[31m\]'
prompt_green='\[\e[32m\]'
prompt_yellow='\[\e[33m\]'
prompt_blue='\[\e[34m\]'
prompt_magenta='\[\e[35m\]'
prompt_cyan='\[\e[36m\]'
prompt_white='\[\e[37m\]'
prompt_default_color='\[\e[0m\]'

# Pieces of the prompt:
prompt_xtitle='\[\e]0;\w\a\]'
prompt_userhost=$prompt_blue'\u@\h'
prompt_cwd=$prompt_yellow'\w'
prompt_time=$prompt_cyan'\t'
prompt_go=$prompt_default_color'$ '

prompt_nocolor='\n\u@\h \w\n$ '
prompt_timeless='\n'$prompt_userhost' '$prompt_cwd'\n'$prompt_go
prompt_full='[bash] '$prompt_userhost' '$prompt_time' '$prompt_cwd' $(parse_git_branch) '$prompt_go
prompt_xterm="$prompt_xtitle""$prompt_full"

# Select prompt based on terminal variable:
case $TERM in
  xterm*|rxvt|screen*|cygwin)
    export PS1=$prompt_xterm ;;
  linux*|*vt100*|cons25)
    export PS1=$prompt_full ;;
  *)
    export PS1=$prompt_nocolor ;;
esac

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
