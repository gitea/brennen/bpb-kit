-- This is my xmonad configuration.  Take it as read that I do not know what I
-- am doing in this file.  It is pure magpie behavior with little underlying
-- logic.  I do not know Haskell.

-- Some sources:
--   Things I have bookmarked: https://pinboard.in/search/u:brennen?query=xmonad
--   Tyler Cipriani's setup: https://github.com/thcipriani/dotfiles/blob/master/xmonad/xmonad.hs
--   Ben LeMasurier's setup: https://github.com/benlemasurier/ben/blob/master/.xmonad/xmonad.hs

--   http://haskell.org/haskellwiki/Xmonad/Config_archive/John_Goerzen%27s_Configuration
--   http://www.xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Doc-Configuring.html
--   http://hackage.haskell.org/packages/archive/xmonad-contrib/0.8.1/doc/html/XMonad-Doc-Extending.html
--   http://hackage.haskell.org/packages/archive/xmonad-contrib/0.8.1/doc/html/XMonad-Actions-CycleWS.html
--   http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Actions-GridSelect.html

import XMonad
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers(isFullscreen, isDialog, doFullFloat, doCenterFloat)
import XMonad.Layout.Accordion
import XMonad.Layout.Fullscreen
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoFrillsDecoration
import XMonad.Util.EZConfig(additionalKeysP) -- Easier keybindings
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows
import XMonad.Util.Run(hPutStrLn)
import XMonad.Util.Run(runInTerm) -- Need this guy for keybindings to external commands
import XMonad.Util.Run(spawnPipe) -- Need this guy for talking to xmobar
import XMonad.Util.Run(unsafeSpawn) -- Need this guy for keybindings to external commands
import XMonad.Util.Scratchpad
import qualified XMonad.StackSet as W -- used by the scratchpad stuff below

-- Tomorrow Night Colors:
colorBackground      = "#1d1f21"
colorCurrent         = "#282a2e"
colorSelection       = "#373b41"
colorForeground      = "#c5c8c6"
colorComment         = "#969896"
colorRed             = "#cc6666"
colorOrange          = "#de935f"
colorYellow          = "#f0c674"
colorGreen           = "#b5bd68"
colorAqua            = "#8abeb7"
colorBlue            = "#81a2be"
colorPurple          = "#b294bb"

-- myTerminal = "uxterm"
-- myTerminal = "gnome-terminal"
-- myTerminal = "alacritty"
myTerminal = "konsole"
-- myTerminal = "kitty"

-- i3lock is a locking mechanism presumably designed originally for i3.
-- It has a nice little input indicator thingy, and with -i can show an
-- image.  phoonlock is just a wrapper that runs phoon to generate an
-- image of the moon phase and then calls i3lock.
myLockScreen = "phoonlock"

-- rofi is a launcher similar to dmenu, with some neat extra features.
-- It's in Debian - apt-get install rofi
myLauncher = "rofi -show run -font 'mono 14'"

manageScratchPad :: ManageHook
manageScratchPad = scratchpadManageHookDefault

myManageHook = composeAll
    [ resource  =? "desktop_window" --> doIgnore
    , className =? "Gimp"          --> doFloat
    , className =? "Vncviewer"     --> doFloat
    -- , className =? "Nautilus"      --> doFloat
    , className =? "Svkbd"         --> doFloat
    , className =? "Google-chrome" --> doShift "web"
    , className =? "Firefox"       --> doShift "web"
    , isFullscreen                 --> doFullFloat
    , namedScratchpadManageHook scratchpads
    ]

-- Named scratchpads - a terminal and a tmux session for editing notes in.  I
-- use gnome-terminal with a role for both of these instead of uxterm with a
-- title, because title gets changed by running stuff inside of xterms,
-- seemingly no matter how things are configured.
--
-- NamedScratchpad docs:
-- https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Util-NamedScratchpad.html
--
-- If you want a regular window here instead of a floaty one, use
-- `(nonFloating)` instead of (customFloating ...).  It turns out that the
-- floating ones are a lot easier to toggle over a workspace without otherwise
-- messing with window management, since you don't have to muck around with
-- layouts at all, size is predictable, etc.  You probably want floating.
--
-- See also /bin/notesession.

scratchpads =
    [ NS "scratch" "gnome-terminal --role scratch" (role =? "scratch")
        -- this goes: start-x, start-y, width (1.0 is full screen), height:
        (customFloating $ W.RationalRect 0.0 0.0 1.0 0.5)
    , NS "notes" "gnome-terminal --role=notesession -e notesession" (role =? "notesession")
        (customFloating $ W.RationalRect 0.2 0.03 0.6 0.90)
    ] where role = stringProperty "WM_WINDOW_ROLE"

myTabConfig = def
    { inactiveBorderColor = colorBackground
    , activeBorderColor = colorPurple
    , activeColor = colorCurrent
    , inactiveColor = colorBackground
    , inactiveTextColor = colorComment
    , activeTextColor = colorForeground
    , fontName = "xft: Droid Sans 14"
    , decoHeight = 25
    }

myLayout = avoidStruts
    $ toggleLayouts tiledSpace
    $ smartBorders
    $ basicRotate
  where
    basicRotate = tabbedBottom shrinkText myTabConfig |||
                  fullTiled |||
                  Mirror fullTiled
                  --- Full |||
                  -- ||| Grid
                  -- ||| ThreeColMid 1 (3/100) (1/2)

    tiledSpace = spacing 60 $ ResizableTall nmaster delta ratio []
    fullTiled = ResizableTall nmaster delta ratio []

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio = toRational (2/(1 + sqrt 5 :: Double))

    -- Percent of screen to increment by when resizing panes
    delta = 5/100

-- A breakdown of desired workspaces:
-- [NSP]  - named scratchpads; created implicitly by namedScratchpad stuff
--          - one gnome-terminal
--          - one gnome-terminal with a tmux containing a vim with notes
-- ...plus:
myWorkspaces =
    [ "top"    -- monitoring, logs, and remote shells
    , "mail"   -- originally for thunderbird, now a rarely-used spare
    , "irc"    -- chat clients (weechat, hipchat, slack, signal, etc.)
    , "code"   -- development and writing
    , "browse" -- web browser(s)
    , "org"    -- a vim with notes file and such
    , "media"  -- photos, graphics work, video, file managers, etc.
    ]

-- ewmh is Extended Window Manager Hints, useful for wmctrl(1):
-- https://en.wikipedia.org/wiki/Extended_Window_Manager_Hints
main = do
  config_with_xmobar <- statusBar myBar myPP toggleStrutsKey defaults
  xmonad $ ewmh config_with_xmobar {
    manageHook = manageDocks <+> myManageHook,
    modMask = mod4Mask
  }

-- xmobar setup:
-- https://wiki.archlinux.org/index.php/Xmonad#More_configurable
-- https://stackoverflow.com/questions/21218309/how-to-use-dzen-instead-of-xmobar-in-this-unusual-setup#21445159
myBar = "xmobar"
myPP = xmobarPP { ppCurrent = xmobarColor "green" "" . wrap "@" "" . shorten 50 }

-- mod-b to toggle xmobar visibility:
toggleStrutsKey XConfig { XMonad.modMask = modMask } = (modMask, xK_b)

defaults = def {
      terminal           = myTerminal
    , borderWidth        = 3
    , normalBorderColor  = colorGreen
    , focusedBorderColor = colorBlue
    , layoutHook         = smartBorders $ myLayout
    , manageHook         = manageScratchPad <+> myManageHook
    , workspaces         = myWorkspaces
    , handleEventHook    = XMonad.Hooks.EwmhDesktops.fullscreenEventHook
} `additionalKeysP`
    -- http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Util-EZConfig.html
    [ ("M-p", spawn myLauncher)
    -- , ("M-s",   scratchpadSpawnActionTerminal $ myTerminal)
    , ("M-S-l", spawn myLockScreen)
    , ("M-S-<Down>", shiftToNext)
    , ("M-S-<Up>", shiftToPrev)
    , ("M-S-<Right>", shiftNextScreen)
    , ("M-S-<Left>", shiftPrevScreen)
    , ("M-z", toggleWS)
    , ("M-g", goToSelected def)
    , ("M-S-g", spawn "jump-to-window")

    -- The following could use just nextWS / prevWS, but I got tired of
    -- workspaces swapping between displays when I cycled through with
    -- arrow keys; this version avoids that entirely.
    -- Move to the next/prev workspaces not already visible on a screen:
    , ("M-<Left>", moveTo Prev hiddenWS)
    , ("M-<Right>", moveTo Next hiddenWS)

    -- Invert screen colors
    -- Formerly used https://github.com/zoltanp/xrandr-invert-colors
    -- ...but xcalib is available from a Debian package
    -- can be helpful for legibility in really bright or dark environments:
    , ("M-i", spawn "xcalib -i -a")

    -- Screenshots:
    , ("<Print>", spawn "grab")

    -- A basic scratchpad, by analogy to the ~ console in Quake:
    , ("M-`", namedScratchpadAction scratchpads "scratch")

    -- Get the current datestamp into the clipboard:
    , ("M-d", spawn "fragment-date | xsel -pb")
    , ("M-S-d", spawn "date -Is | tr -d '\n' | xsel -pb")

    -- Notes:
    , ("M-S-n", namedScratchpadAction scratchpads "notes")
    , ("M-n", namedScratchpadAction scratchpads "notes")

    -- Special laptopish media keys:
    , ("<XF86AudioMute>",         spawn "amixer set Master toggle")
    , ("<XF86AudioLowerVolume>",  spawn "amixer set Master 2%- unmute")
    , ("<XF86AudioRaiseVolume>",  spawn "amixer set Master 2%+ unmute")
    , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 5")
    , ("<XF86MonBrightnessUp>",   spawn "xbacklight -inc 5")
    ]
