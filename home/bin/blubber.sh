#!/usr/bin/env bash
if [ $# -lt 2 ]; then
  echo 'Usage: blubber config.yaml variant'
  return 1
fi
curl -s -H 'content-type: application/yaml' --data-binary @"$1" https://blubberoid.wikimedia.org/v1/"$2"
