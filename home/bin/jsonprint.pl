#!/usr/bin/perl

use strict;
use warnings;

use 5.10.0;
use JSON;
use IO::Handle;

my $coder = JSON::XS->new->utf8->pretty->allow_nonref;

STDOUT->autoflush(1);

while (my $record = <STDIN>) {
  chomp ($record);
  my $decoded = $coder->decode($record);
  say $coder->encode($decoded->{message});
}
