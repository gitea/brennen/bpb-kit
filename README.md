bpb-kit
=======

**The canonical copy of this repository [lives on code.p1k3.com][bpb-kit].**

This repo is for dotfiles, utility scripts, and other things in my personal
setup.  Ideally, it includes most of the bits and pieces that make a personal
machine usable for me.  It occasionally serves as a testbed for things that
become standalone projects, or get cycled into stuff I do for a living.

I generally use Debian and Debian-like GNU/Linux systems (sometimes including
Ubuntu), with the [xmonad][xmonad] tiling window manager rather than a full
desktop environment like Gnome or KDE.  I usually edit text in Vim, with
[VimWiki][vimwiki] for notes.  I maintain a website called [p1k3][p1k3] using
[wrt][wrt].

I have a [partially-finished book about the command line][userland] which may
be relevant.

status
------

As of July 2022, this collection is actively maintained, although it doesn't
meet the standards of quality, consistency, or documentation you'd want from a
real software project.

(Then again, neither do most software projects.)

copying
-------

Except as noted below or in the body of specific files, and to the extent
possible under law, I, Brennen Bearnes, waive all copyright and related or
neighboring rights to the contents of bpb-kit.  Go to town.

a philosophical note about copying
----------------------------------

In the _general case_, I've long tended to use strong copyleft licenses like
the GNU General Public License.  Where I have the option, I'll generally
license more substantial projects with a view towards impeding the corporate
capture of every available computational resource and generally throwing a
little grit in the gears of capital in these latter days before it finishes
murdering the entire biosphere.  (No one uses my software, so this is a purely
symbolic gesture.)

All that said, this is a trivial repository of configuration fragments and
utility scripts for a limited audience.  It will do the most good if nobody has
to worry about the bureaucracy of grabbing bits and pieces of it.

From time to time, I _may_ incorporate code from other sources in this
collection, as long as it's in the public domain or under a license that meets
the Debian Free Software Guidelines.  In the latter case, I'll do my best to
ensure that said code is clearly marked.

sources
-------

I have thiefed ideas and fragments of configuration from, at least, the
following projects:

  - [Advanced Bash Scripting Guide][bash-guide], Mendel Cooper
  - [Learn Vimscript the Hard Way][vimscript], Steve Losh
  - [ArchWiki][archwiki]

And people:

  - [Alan][alan]
  - Ahmon
  - [Ben][ben]
  - [Casey][casey]
  - [Chris][chris]
  - [Dave][dave]
  - [Jacky][jacky-alcine]
  - [Leah][leah]
  - [Lars][lars]
  - Mukunda
  - [Rob][rob]
  - [Todd][todd]
  - [Tyler][tyler]
  - [Victoria][victoria-drake]

contents
--------

### a note about languages

Apart from shell, I tend to reach for Perl for smalltime local utilities.  It's
a language I've been comfortable in for 20 years, it offers a decent stdlib and
a ton of good libraries by way of CPAN, and it's installed pretty much
everywhere.  It's also extremely honed for string manipulation, which in
practice is the technique that glues most of my nonsense together.

You might also find examples of Python, PHP, Ruby, awk, JavaScript, HTML/CSS,
etc.

### shell stuff

At this writing I use ZSH on personal systems and Bash when writing tutorials,
doing tech support, or working on production server systems.  I like to keep
lots and lots of history.  I think shell scripting is in many ways a nightmare
for programs longer than a few lines, but I do it pretty often anyway.

  - [`.bashrc`](home/.bashrc)
  - [`.zshrc`](home/.zshrc) - nothing fancy
  - [`.sh_common`](home/.sh_common) - aliases and variables for both Bash and ZSH;
    no Bash compatibility guarantees here, since I mostly don't use custom
    aliases in Bash

### scripts in [home/bin/](home/bin)

Scripts here fall into a handful of categories:

  - Short wrappers for getting (or remembering) specific behavior from other
    tools
  - Screenshot and screencast tools, used at one time in my work as a writer
    for DigitalOcean and Adafruit and kept because I got used to the
    convenience
  - Things for working with the [p1k3][p1k3] repo and site
  - Text filters and template fragments, mostly for use with Vim aliases
  - Stuff I use for working with my personal notes
  - Miscellaneous utilities

Many of these are unlikely to be portable, useful, or documented.

TODO: Migrate these descriptions to the scripts themselves so they can be
automatically extracted.

  - `cheat`: a place to hang a personal cheatsheet of sorts
  - `chrome-incognito`: run Google Chrome in incognito mode
  - `dmenu_unique`: run dmenu with big fonts and vertical, only showing each entry once
  - `dog`: concatenate argument strings with stdin
  - `edit-clipboard`: edit the current text selection in `$EDITOR` (requires `xclip`)
  - `enterprise`: play a sound like the _USS Enterprise_ engines in TNG
  - `filter-decorate`: splat some text dingbats into HTML I write sometimes
  - `filter-exec`: replace text in-between markers with result of shell-script execution
  - `filter-exec-raw`: like above, but different
  - `filter-exec-stdin`: feed text in-between markers to standard input of a command, replace it with output
  - `filter-markdownify`: convert a few things in old DigitalOcean tutorials to Markdown
  - `filter-vertical`: verticalize a string
  - `firefox-fromselection`: open a selected url in firefox
  - `fragment-bullet`: print out a "random" dingbat character
  - `fragment-entry`: stub blog entry
  - `fragment-entry-gallery`: stub blog entry with gallery
  - `fragment-entry-poem`: stub blog entry with poem
  - `get-external-ip`: print public IP address
  - `gif-sel`, `gif-sel-4`, `gif-sel-15`: take an animated gif of selected screen region
  - `git-diff-wrapper`: use vim with `git-diff-tool`
  - `git-local-to-remote-status`
  - `grab`, `grab-sel`: take a screenshot, take a screenshot of a selected region
  - `json_decode.php`: decode JSON into PHP data structures
  - `jsonprint.pl`: pretty-print JSON with Perl
  - `jump-to-window`: use `wmctrl` and `dmenu` to pick a window to jump to
  - `listusers`: print an HTML list of users
  - `lynx-wrapper-edit`: edit a p1k3 entry in vim and re-render everything
  - `machine-status`: use `dsh` to check status on a list of machines (stub)
  - `mostrecent`: print the name of newest file in the current directory
  - `notesession`: start a tmux named session for notes
  - `photocp`: copy photos from various media to a home directory location
  - `pmwhich`: find the on-filesystem location of a Perl module
  - `rightnow`: print the current time in a variety of formats
  - `saytime`: speak the time with Festival
  - `snowday`: is it a snow day for the Boulder Valley School District?
  - `ssh-nofucks`: SSH to an address, I don't really care if the key has changed
  - `st`: get status using myrepos
  - `timelog`: parse a timelogging format (I use this to bill for contracting)
  - `timeslice`: aggregate some data for a given date range or file's implied date
  - `today`: print a date
  - `todaydir`: find a p1k3 dir for the current date
  - `uni`: search unicode codepoint names (via @chneukirchen)
  - `unsorted-unique`: print all lines of input once (just an `awk` one-liner)
  - `wip`: move a p1k3 file into a work-in-progress directory
  - `words`: split input into individual words
  - `wthr`: use unicode snowflakes to display CPU load
  - `xm`: call xmodmap
  - `xmonad.start`: personal version of xmonad startup script
  - `xtfix`: do a subtle color shift within the current xterm
  - `yank`, `put`, `put-mv`: stash a file path, copy or move it to the current directory

### fragments

The [fragments](home/fragments) directory is for code snippets that I write in the
course of testing some idea, checking a technique, or trying to solve a problem
posed by friends.

They're typically the kind of thing I'd throw in a random file called something
like `test.sh` and overwrite later.  It seemed useful to start collecting these
instead.

### cheatsheets

The [cheatsheets](home/cheatsheets) directory is mostly empty, but may become a
repository of useful shorthand documentation for the increasingly-many things I
use that are not discoverable or memorable enough for my deteriorating
long-term memory.

### tmux

I use `tmux` for terminal multiplexing (i.e., most of what GNU Screen does).
In practice, this means that I rely on it for

  - persistent shell sessions, sometimes attached to multiple machines
  - putting a bunch of shell / editor buffers inside one `ssh` or `mosh`
    session
  - capturing and scrolling back through a bunch of output

My [`.tmux.conf`](home/.tmux.conf) is brief, but does contain one useful
snippet for correcting weird Esc-key behavior in Vim.

### vim config

My Vim setup was once written to be easily copied by new users.  It's gotten a
bit out of hand since then.

  - [`vimrc`](home/.vim/vimrc) - see file for installation details
  - uses [Vundle][vundle] to manage plugins
  - pulls in a ton of plugins, some more useful than others
  - has a bunch of customization for [VimWiki][vimwiki]

I don't use Neovim because I've never gotten it to work for my config without
serious glitches.  (At this writing, I had last made a serious attempt in late
2021.)

### window management

  - `.xmodmap`
    - capslock -> ctrl
  - `.xinitrc`
    - sources `.hacksrc` if it exists
  - `.Xresources` tweaks xterm behavior and a number of fonts
    - behavior changes depending on whether `.hacksrc` is linked to
      `.hacksrc_hi_dpi`
  - XMonad config -
    - `.xmobarrc`, `.xmonad/`
    - see [`bin/xmonad.start`](bin/xmonad.start) for a drop-in replacement for
      default XMonad startup on some Debian-like systems, including Gnome/Unity
      stuff and the like. Perpetually not-quite-right.

building a debian package for dependencies
------------------------------------------

See instructions in [ns-control](ns-control).

I haven't done this for a while, so it's probably out of date.

[alan]: https://github.com/acg
[archwiki]: https://wiki.archlinux.org/
[bash-guide]: http://www.tldp.org/LDP/abs/html/
[ben]: https://github.com/benlemasurier
[bpb-kit]: https://code.p1k3.com/gitea/brennen/bpb-kit
[code]: https://code.p1k3.com/
[casey]: https://github.com/caseydentinger/
[chris]: https://github.com/frencil
[dave]: https://stilldavid.com/
[leah]: https://github.com/chneukirchen/
[lars]: https://liw.fi/
[p1k3]: https://p1k3.com/
[rob]: https://github.com/robacarp/
[todd]: https://uniontownlabs.org/
[tyler]: https://tylercipriani.com/
[jacky-alcine]: https://jacky.wtf/
[userland]: https://p1k3.com/userland-book/
[victoria-drake]: https://victoria.dev/
[vimscript]: http://learnvimscriptthehardway.stevelosh.com/
[vimwiki]: https://github.com/vimwiki/vimwiki
[vundle]: https://github.com/VundleVim/Vundle.vim
[wrt]: https://code.p1k3.com/gitea/brennen/wrt
[xmonad]: http://xmonad.org/
