#!/bin/sh

KIT_ROOT=$(pwd)

echo "installing kit from $KIT_ROOT"

echo 'installing package prereqs'
sudo apt-get install build-essential xterm tmux vim perl ack-grep \
git git-gui htop zsh suckless-tools figlet xmonad trayer xmobar \
volumeicon-alsa diodon curl jq byzanz festival redshift-gtk ranger \
rofi xsettingsd wmctrl lynx perl-doc dict moreutils myrepos etckeeper \
bzr cvs darcs mercurial subversion subversion-tools dunst i3lock \
fzf xclip xsel xautolock pngphoon tree scrot pandoc dnsutils \
pasystray pavucontrol exa shellcheck \
  bsdmainutils \
  calendar \
  visidata \
  sqlite3 \
  konsole \
  cabal-install \
  ncdu \
  yubikey-manager \
  mosh \
  xcalib \
  icc-profiles

echo 'installing some extra fonts'
sudo apt-get install fonts-noto-color-emoji fonts-liberation fonts-roboto-hinted texlive-fonts-extra

echo 'installing pandoc from cabal'
# TODO: restore this or put it elsewhere
# cabal update
# cabal install pandoc --lib pandoc pandoc

echo 'installing perl dependencies'
# TODO: Some of these belong with specific progs:
sudo cpan -i Module::Build App::WRT DBI SQL::Abstract DBD::SQLite XML::FeedPP

# TODO: These need to go in a venv now?
echo 'installing python dependencies and utils'
sudo apt-get install python3-pip

if [ ! -d ~/python_bullshit ]; then
  python3 -m venv ~/python_bullshit
  ~/python_bullshit/bin/pip3 install panflute
  ~/python_bullshit/bin/pip3 install mastodon-archive
  ~/python_bullshit/bin/pip3 install pinboard-to-sqlite
  ~/python_bullshit/bin/pip3 install virtualenv
fi

echo 'vim setup'
  if [ -e ~/.vim ]; then
    echo 'a ~/.vim exists'
  else
    ln -s "$KIT_ROOT/home/.vim" "$HOME/.vim"
  fi

  mkdir -p ~/.vim/bundle
  if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  fi
  vim +PluginInstall

echo 'xmonad setup'
echo "(don't forget to point /usr/share/xsessions/xmonad.desktop at xmonad.start)"
  if [ ! -d ~/.xmonad ]; then
    ln -s "$KIT_ROOT/home/.xmonad" "$HOME/.xmonad"
  fi
  if [ ! -f ~/.xmobarrc ]; then
    ln -s "$KIT_ROOT/home/.xmobarrc" "$HOME/.xmobarrc"
  fi
  if [ ! -f ~/.xsettingsd ]; then
    ln -s "$KIT_ROOT/home/.xsettingsd" "$HOME/.xsettingsd"
  fi

# shellcheck disable=SC2088
echo '~/bin'
  if [ ! -d ~/bin ]; then
    ln -s "$KIT_ROOT/home/bin" "$HOME/bin"
  fi

# shellcheck disable=SC2088
echo '~/fragments'
  if [ ! -d ~/fragments ]; then
    ln -s "$KIT_ROOT/home/fragments" "$HOME/fragments"
  fi

# shellcheck disable=SC2088
echo '~/cheatsheets'
  if [ ! -d ~/cheatsheets ]; then
    ln -s "$KIT_ROOT/home/cheatsheets" "$HOME/cheatsheets"
  fi

echo 'shell setup'
  if [ ! -f ~/.zshrc ]; then
    ln -s "$KIT_ROOT/home/.zshrc" "$HOME/.zshrc"
  fi
  if [ ! -f ~/.bashrc ]; then
    ln -s "$KIT_ROOT/home/.bashrc" "$HOME/.bashrc"
  fi
  if [ ! -f ~/.sh_common ]; then
    ln -s "$KIT_ROOT/home/.sh_common" "$HOME/.sh_common"
  fi

echo 'tmux / screen setup'
  if [ ! -f ~/.tmux.conf ]; then
    ln -s "$KIT_ROOT/home/.tmux.conf" "$HOME/.tmux.conf"
  fi
  if [ ! -f ~/.screenrc ]; then
    ln -s "$KIT_ROOT/home/.screenrc" "$HOME/.screenrc"
  fi

echo 'git setup'
  if [ ! -f ~/.gitconfig ]; then
    ln -s "$KIT_ROOT/home/.gitconfig" "$HOME/.gitconfig"
  fi

echo 'lynx setup'
  if [ ! -f ~/.lynxrc ]; then
    ln -s "$KIT_ROOT/home/.lynxrc" "$HOME/.lynxrc"
  fi

echo 'keyboard setup (Ctrl and CapsLock swap)'
  sudo cp etc/default/keyboard /etc/default/keyboard
  # TODO: This worked on Debian 12 to trigger the new setup - I _think_.
  #       Not sure if it's taken on a reboot or if I need to add a rule
  #       or something to run each time.
  #
  #       Sources: https://wiki.debian.org/Keyboard
  sudo udevadm trigger --subsystem-match=input --action=change

echo 'Remember to:'
echo '  - install commandlog from https://code.p1k3.com/gitea/brennen/commandlog'
echo '  - chsh'
